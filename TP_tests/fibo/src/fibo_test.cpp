//
// Created by erick on 27/04/2020.
//

#include <CppUTest/CommandLineTestRunner.h>
#include "Fibo.hpp"

TEST_GROUP(GroupFibo){};

TEST(GroupFibo, checkNumber) {

    CHECK_EQUAL(0, fibo(0));
    CHECK_EQUAL(1, fibo(1));
    CHECK_EQUAL(1, fibo(2));
    CHECK_EQUAL(2, fibo(3));
    CHECK_EQUAL(3, fibo(4));
    CHECK_EQUAL(5, fibo(5));
    CHECK_EQUAL(8, fibo(6));
    CHECK_EQUAL(13, fibo(7));
}

TEST(GroupFibo, exception) {
    CHECK_THROWS(std::string, fibo(51));
    CHECK_THROWS(std::string, fibo(-1));
}
