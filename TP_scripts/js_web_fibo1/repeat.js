function repeatN(n, f) {
    var ligne = [];
    for (var i = 0; i < n; ++i) {
        ligne[i] = f(i);
    }
    return ligne;
}
