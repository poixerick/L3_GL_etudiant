const { app, BrowserWindow } = require('electron')

function fiboIterative (n) {
    if (n < 2)
        return n;
    else {
        let f0 = 0;
        let f1 = 1;
        for (let i=1; i<n; i++) {
            const tmp = f1+f0;
            f0 = f1;
            f1 = tmp;
        }
        return f1;
    }
}

function createWindow () {
    let win = new BrowserWindow({
        width: 600,
        height: 600,
        webPreferences: { nodeIntegration: true }
    })

    win.loadFile('index.html')

    // SHOW DEV TOOLS
    win.webContents.openDevTools()

    win.on('closed', () => { win = null })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

